# README 

> Ingestion Microservice for Elasticsearch Data Loading using Avro Schema as the data definition contract

# Introduction

The project is a reference application of a Staging Microservice capable of loading file based data and populates them into an Operational Read Datastore (ORDS). The domain data schema is based on Avro JSON Schemas and for the persistency of the Avro JSON documents Elasticsearch is used. 

The Microservice is based on the Hexagon architecture pattern and uses the following implementation technologies/framworks

* based on [SpringBoot App](https://spring.io/projects/spring-boot) 
* using the [Apache Camel](https://camel.apache.org/) integration framework which supports various Enterprise integration patterns.

Refer to the following article to get an overview about Camel used in combination with Hexagon.

* [Hexagonal Architecture as Natural Fit for Camel](https://developers.redhat.com/blog/2017/12/05/hexagonal-architecture-natural-fit-apache-camel/)

# Guidance

* The Avro Schema can be found in `resources/avro/`
* The generated classes will be in `java/clb`

# Additional Information

## Data Sets

* Covid-19 Coronavirus EU Open Data Portal: Data on the geographic distribution of COVID-19 cases worldwide
  * [Data Set URL](https://data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data/resource/55e8f966-d5c8-438e-85bc-c7a5a26f4863) or
    [Data Set URL 2](https://www.ecdc.europa.eu/en/publications-data/download-todays-data-geographic-distribution-covid-19-cases-worldwide)
 released under [Creative Commons 4.0](https://creativecommons.org/licenses/by/4.0/)
* COVID-19 Finance Sector Related Policy Responses by the World Bank.
    *  [Data Set URL](https://www.ecdc.europa.eu/en/publications-data/download-todays-data-geographic-distribution-covid-19-cases-worldwide) 
       released under [Creative Commons 4.0](https://creativecommons.org/licenses/by/4.0/)
  

## Date and Time Handling

The Microservice will use `java.util.time` for managing date and time attributes which are part of the supported Bindy file attribute definitions.

In context of Timestamps the Microcservice has to know the time zone in which the inbound system has produced its timestamp. It will convert it to a UTC timestamp and pass the Timezone information as part of the Header information of the Avro Schema. In case of an unknown timezone, the value `unknown`will be passed in the header. 

In our interface specification file  (can be found in `interfacespec\*.csv`), we use the following Date and Time definitions:

* `Date`: Represents a date within the calendar, with no reference to a particular time zone or time of day.
  * Maps in Avro Schema to an `int` with `logical type date`.  Stores the number of days from the unix epoch, 1 January 1970 (ISO calendar).
* `TimeMillis`
  * Represents a time of day, with no reference to a particular calendar, time zone or date, with a precision of one millisecond.
  * Maps in Avro Schema to an `long`with `logicalType time-millis`.  Stores the number of microseconds after midnight, `00:00:00.000000` 
* `TimeMicros`
    * Represents a time of day, with no reference to a particular calendar, time zone or date, with a precision of one microseconds.
    * Maps in Avro Schema to an `long` with `logicalType time-micros`. Stores the number of microseconds after midnight, `00:00:00`
* `TimestampMillis`
  *  Represents an instant on the global timeline, independent of a particular time zone or calendar, with a precision of one millisecond
  *  Maps in Avro Schema to an `long` with `logicalType timestamp-millis`. Stores the number of microseconds, where the long stores the number of milliseconds from the `unix epoch, 1 January 1970 00:00:00.000 UTC` .
* `TimestampMicros`
  * Represents an instant on the global timeline, independent of a particular time zone or calendar, with a precision of one millisecond
  * Maps in Avro Schema to an `long` with `logicalType timestamp-micros`. Stores the number of microseconds, where the long stores the number of milliseconds from the `unix epoch, 1 January 1970 00:00:00.000000 UTC` .
* `LocalTimestampMillis` 
  * Represents a timestamp in a local timezone, regardless of what specific time zone is considered local, with a precision of one millisecond. 
  * The Microservice will transform loaded Timestamp to the timezone `UTC`. 
  * The processing time zone of the master system is part of the meta information known to the Microservice (`application.yml`).
  * The timezone is part of the `header`information within the Avro Schema and defaults to `UTC`
  * Maps in Avro Schema to an `long` with `logicaltype local-timestamp-millis`. Long stores the number of milliseconds, from `1 January 1970 00:00:00.000 UTC`. 
* `LocalTimestampMicros`
  * Represents a timestamp in a local timezone, regardless of what specific time zone is considered local, with a precision of one microseconds. 
  * The Microservice will transform loaded Timestamp to the timezone `UTC`. 
  * The processing time zone of the master system is part of the meta information known to the Microservice (`application.yml`).
  * The timezone is part of the `header`information within the Avro Schema and defaults to `UTC``
  * Java 8 doesn't support in the `java.time.*`API epoch calculation on microseconds level, therfore we will not map them in the Avro Schema to an `long`, where the long stores the number of microseconds, from `1 January 1970 00:00:00.000000 UTC`.
  * We will create in the Avro Schema for the relevant attribute of name `<nameTSD>` two attributes 
    * `<nameTSD>`of Avro type `String` storing the date string in `ISO8901` pattern (`2018-03-09T21:03:33.123456Z`)  
    * `<nameTSDmillis>` of Avro type `long` with `logcialType local-timestamp-millis`

* `LocalTimestampMicrosFixLowValue`
  * Represents a timestamp in a local timezone, regardless of what specific time zone is considered local, with a precision of one microseconds. 
  * The Microservice will transform loaded Timestamp to the timezone `UTC`. 
  * The processing time zone of the master system is part of the meta information known to the Microservice (`application.yml`).
  * The timezone is part of the `header`information within the Avro Schema and defaults to `UTC``
  * **Fix Rule:** In case the passed in Year is `0000` the entry will be transformed to `0001-01-01T00:00Z`. Will only work for date patterns which starts with the `yyyy`
  * Java 8 doesn't support in the `java.time.*`API epoch calculation on microseconds level, therfore we will not map them in the Avro Schema to an `long`, where the long stores the number of microseconds, from `1 January 1970 00:00:00.000000 UTC`.
  * We will create in the Avro Schema for the relevant attribute of name `<nameTSD>` two attributes 
    * `<nameTSD>`of Avro type `String` storing the date string in `ISO8901` pattern (`2018-03-09T21:03:33.123456Z`)  
    * `<nameTSDmillis>` of Avro type `long` with `logcialType local-timestamp-millis`

### Date/Time pattern

In the interface specification file you can provide patterns for the time and date format which is used in the inboud route. The pattern are based on the `java.time.format.DateTimeFormatter` specification ([API Doc](https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html#ISO_LOCAL_DATE)), which comes with multiple predefined date/time formats that follow ISO and RFC standards.


### Example

#### Example 1

``` java
  System.out.println("Input: 2018-03-09-21.03.33.123456 with formatter 'yyyy-MM-dd-HH.mm.ss.SSSSSS'");
  LocalDateTime dt = LocalDateTime.parse("2018-03-09-21.03.33.123456",DateTimeFormatter.ofPattern("yyyy-MM-dd-HH.mm.ss.SSSSSS"));
  ZonedDateTime zt = ZonedDateTime.of(dt, ZoneId.of("UTC"));
  System.out.println("Zonetime Epoch (Milliseconds): "+ zt.toInstant().toEpochMilli());
  System.out.println("ZoneDateTime UTC:"+zt.toString());
  System.out.println("ZoneDateTime UTC ISO8601:"+zt.toOffsetDateTime().toString());
```


    Input: 2018-03-09-21.03.33.123456 with formatter 'yyyy-MM-dd-HH.mm.ss.SSSSSS'
    Zonetime Epoch (Milliseconds): 1520629413123
    ZoneDateTime UTC:2018-03-09T21:03:33.123456Z[UTC]
    ZoneDateTime UTC ISO8601:2018-03-09T21:03:33.123456Z
    ZoneDateTime  MEZ  Epoch (Milliseconds): 1520625813123
    ZoneDatime MEZ:2018-03-09T21:03:33.123456+01:00[Europe/Zurich]
    ZoneDateTime MEZ ISO8601:2018-03-09T21:03:33.123456+01:00


#### Example 2: Date Low Value
Be aware that epoch value is negative.

``` java
  Input: 0001-01-01-00.00.00.000000 with formatter 'yyyy-MM-dd-HH.mm.ss.SSSSSS'
  Zonetime Epoch (Milliseconds): -62135596800000
  ZoneDateTime UTC:0001-01-01T00:00Z[UTC]
  ZoneDateTime UTC ISO8601:0001-01-01T00:00Z
```


## References: 

*   https://www.baeldung.com/apache-camel-spring-boot
*   https://www.baeldung.com/apache-camel-intro
*   https://www.baeldung.com/configuration-properties-in-spring-boot
*   https://www.jetbrains.com/help/idea/http-client-in-product-code-editor.html#creating-http-request-files
*   https://www.elastic.co/webinars/moving-from-on-prem-to-managed-services-with-elastic-on-azure?blade=kibanafeed
*   https://www.elastic.co/guide/en/elasticsearch/reference/current/removal-of-types.html#:~:text=Since%20the%20first%20release%20of,type%20and%20a%20tweet%20type.
*   https://access.redhat.com/documentation/en-us/red_hat_jboss_fuse/6.3/html/fuse_integration_services_2.0_for_openshift/camel-spring-boot
*   https://www.sourceallies.com/2014/01/how-to-implement-the-splitter-and-aggregator-patterns-with-apache-camel/
*   https://stackoverflow.com/questions/39586311/java-8-localdatetime-now-only-giving-precision-of-milliseconds
