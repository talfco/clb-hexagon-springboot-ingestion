/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package n1z.covidweeklycaserecord;

import org.apache.avro.specific.SpecificData;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@SuppressWarnings("all")
/** Length:  precision: 19 */
@org.apache.avro.specific.AvroGenerated
public class Rate14Day extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = -5982365760187012351L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"Rate14Day\",\"namespace\":\"n1z.covidweeklycaserecord\",\"doc\":\"Length:  precision: 19\",\"fields\":[{\"name\":\"value\",\"type\":\"float\",\"logicalType\":\"float\"}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<Rate14Day> ENCODER =
      new BinaryMessageEncoder<Rate14Day>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<Rate14Day> DECODER =
      new BinaryMessageDecoder<Rate14Day>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   */
  public static BinaryMessageDecoder<Rate14Day> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   */
  public static BinaryMessageDecoder<Rate14Day> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<Rate14Day>(MODEL$, SCHEMA$, resolver);
  }

  /** Serializes this Rate14Day to a ByteBuffer. */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /** Deserializes a Rate14Day from a ByteBuffer. */
  public static Rate14Day fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

   private float value;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public Rate14Day() {}

  /**
   * All-args constructor.
   * @param value The new value for value
   */
  public Rate14Day(java.lang.Float value) {
    this.value = value;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return value;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: value = (java.lang.Float)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'value' field.
   * @return The value of the 'value' field.
   */
  public java.lang.Float getValue() {
    return value;
  }


  /**
   * Creates a new Rate14Day RecordBuilder.
   * @return A new Rate14Day RecordBuilder
   */
  public static n1z.covidweeklycaserecord.Rate14Day.Builder newBuilder() {
    return new n1z.covidweeklycaserecord.Rate14Day.Builder();
  }

  /**
   * Creates a new Rate14Day RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new Rate14Day RecordBuilder
   */
  public static n1z.covidweeklycaserecord.Rate14Day.Builder newBuilder(n1z.covidweeklycaserecord.Rate14Day.Builder other) {
    return new n1z.covidweeklycaserecord.Rate14Day.Builder(other);
  }

  /**
   * Creates a new Rate14Day RecordBuilder by copying an existing Rate14Day instance.
   * @param other The existing instance to copy.
   * @return A new Rate14Day RecordBuilder
   */
  public static n1z.covidweeklycaserecord.Rate14Day.Builder newBuilder(n1z.covidweeklycaserecord.Rate14Day other) {
    return new n1z.covidweeklycaserecord.Rate14Day.Builder(other);
  }

  /**
   * RecordBuilder for Rate14Day instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<Rate14Day>
    implements org.apache.avro.data.RecordBuilder<Rate14Day> {

    private float value;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(n1z.covidweeklycaserecord.Rate14Day.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.value)) {
        this.value = data().deepCopy(fields()[0].schema(), other.value);
        fieldSetFlags()[0] = true;
      }
    }

    /**
     * Creates a Builder by copying an existing Rate14Day instance
     * @param other The existing instance to copy.
     */
    private Builder(n1z.covidweeklycaserecord.Rate14Day other) {
            super(SCHEMA$);
      if (isValidValue(fields()[0], other.value)) {
        this.value = data().deepCopy(fields()[0].schema(), other.value);
        fieldSetFlags()[0] = true;
      }
    }

    /**
      * Gets the value of the 'value' field.
      * @return The value.
      */
    public java.lang.Float getValue() {
      return value;
    }

    /**
      * Sets the value of the 'value' field.
      * @param value The value of 'value'.
      * @return This builder.
      */
    public n1z.covidweeklycaserecord.Rate14Day.Builder setValue(float value) {
      validate(fields()[0], value);
      this.value = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'value' field has been set.
      * @return True if the 'value' field has been set, false otherwise.
      */
    public boolean hasValue() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'value' field.
      * @return This builder.
      */
    public n1z.covidweeklycaserecord.Rate14Day.Builder clearValue() {
      fieldSetFlags()[0] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Rate14Day build() {
      try {
        Rate14Day record = new Rate14Day();
        record.value = fieldSetFlags()[0] ? this.value : (java.lang.Float) defaultValue(fields()[0]);
        return record;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<Rate14Day>
    WRITER$ = (org.apache.avro.io.DatumWriter<Rate14Day>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<Rate14Day>
    READER$ = (org.apache.avro.io.DatumReader<Rate14Day>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

}
