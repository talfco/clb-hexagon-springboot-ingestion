/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package n1z.covidweeklycaserecord;

import org.apache.avro.specific.SpecificData;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@SuppressWarnings("all")
/** Length: Length: 3 */
@org.apache.avro.specific.AvroGenerated
public class WeeklyCount extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = 8532886803322757979L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"WeeklyCount\",\"namespace\":\"n1z.covidweeklycaserecord\",\"doc\":\"Length: Length: 3\",\"fields\":[{\"name\":\"value\",\"type\":\"long\"}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<WeeklyCount> ENCODER =
      new BinaryMessageEncoder<WeeklyCount>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<WeeklyCount> DECODER =
      new BinaryMessageDecoder<WeeklyCount>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   */
  public static BinaryMessageDecoder<WeeklyCount> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   */
  public static BinaryMessageDecoder<WeeklyCount> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<WeeklyCount>(MODEL$, SCHEMA$, resolver);
  }

  /** Serializes this WeeklyCount to a ByteBuffer. */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /** Deserializes a WeeklyCount from a ByteBuffer. */
  public static WeeklyCount fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

   private long value;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public WeeklyCount() {}

  /**
   * All-args constructor.
   * @param value The new value for value
   */
  public WeeklyCount(java.lang.Long value) {
    this.value = value;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return value;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: value = (java.lang.Long)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'value' field.
   * @return The value of the 'value' field.
   */
  public java.lang.Long getValue() {
    return value;
  }


  /**
   * Creates a new WeeklyCount RecordBuilder.
   * @return A new WeeklyCount RecordBuilder
   */
  public static n1z.covidweeklycaserecord.WeeklyCount.Builder newBuilder() {
    return new n1z.covidweeklycaserecord.WeeklyCount.Builder();
  }

  /**
   * Creates a new WeeklyCount RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new WeeklyCount RecordBuilder
   */
  public static n1z.covidweeklycaserecord.WeeklyCount.Builder newBuilder(n1z.covidweeklycaserecord.WeeklyCount.Builder other) {
    return new n1z.covidweeklycaserecord.WeeklyCount.Builder(other);
  }

  /**
   * Creates a new WeeklyCount RecordBuilder by copying an existing WeeklyCount instance.
   * @param other The existing instance to copy.
   * @return A new WeeklyCount RecordBuilder
   */
  public static n1z.covidweeklycaserecord.WeeklyCount.Builder newBuilder(n1z.covidweeklycaserecord.WeeklyCount other) {
    return new n1z.covidweeklycaserecord.WeeklyCount.Builder(other);
  }

  /**
   * RecordBuilder for WeeklyCount instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<WeeklyCount>
    implements org.apache.avro.data.RecordBuilder<WeeklyCount> {

    private long value;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(n1z.covidweeklycaserecord.WeeklyCount.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.value)) {
        this.value = data().deepCopy(fields()[0].schema(), other.value);
        fieldSetFlags()[0] = true;
      }
    }

    /**
     * Creates a Builder by copying an existing WeeklyCount instance
     * @param other The existing instance to copy.
     */
    private Builder(n1z.covidweeklycaserecord.WeeklyCount other) {
            super(SCHEMA$);
      if (isValidValue(fields()[0], other.value)) {
        this.value = data().deepCopy(fields()[0].schema(), other.value);
        fieldSetFlags()[0] = true;
      }
    }

    /**
      * Gets the value of the 'value' field.
      * @return The value.
      */
    public java.lang.Long getValue() {
      return value;
    }

    /**
      * Sets the value of the 'value' field.
      * @param value The value of 'value'.
      * @return This builder.
      */
    public n1z.covidweeklycaserecord.WeeklyCount.Builder setValue(long value) {
      validate(fields()[0], value);
      this.value = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'value' field has been set.
      * @return True if the 'value' field has been set, false otherwise.
      */
    public boolean hasValue() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'value' field.
      * @return This builder.
      */
    public n1z.covidweeklycaserecord.WeeklyCount.Builder clearValue() {
      fieldSetFlags()[0] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public WeeklyCount build() {
      try {
        WeeklyCount record = new WeeklyCount();
        record.value = fieldSetFlags()[0] ? this.value : (java.lang.Long) defaultValue(fields()[0]);
        return record;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<WeeklyCount>
    WRITER$ = (org.apache.avro.io.DatumWriter<WeeklyCount>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<WeeklyCount>
    READER$ = (org.apache.avro.io.DatumReader<WeeklyCount>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

}
