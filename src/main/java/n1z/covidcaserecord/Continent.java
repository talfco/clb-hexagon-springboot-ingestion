/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package n1z.covidcaserecord;

import org.apache.avro.specific.SpecificData;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@SuppressWarnings("all")
/** Length: Length: 15 */
@org.apache.avro.specific.AvroGenerated
public class Continent extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = 890294739938186973L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"Continent\",\"namespace\":\"n1z.covidcaserecord\",\"doc\":\"Length: Length: 15\",\"fields\":[{\"name\":\"value\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"}}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<Continent> ENCODER =
      new BinaryMessageEncoder<Continent>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<Continent> DECODER =
      new BinaryMessageDecoder<Continent>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   */
  public static BinaryMessageDecoder<Continent> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   */
  public static BinaryMessageDecoder<Continent> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<Continent>(MODEL$, SCHEMA$, resolver);
  }

  /** Serializes this Continent to a ByteBuffer. */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /** Deserializes a Continent from a ByteBuffer. */
  public static Continent fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

   private java.lang.String value;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public Continent() {}

  /**
   * All-args constructor.
   * @param value The new value for value
   */
  public Continent(java.lang.String value) {
    this.value = value;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return value;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: value = (java.lang.String)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'value' field.
   * @return The value of the 'value' field.
   */
  public java.lang.String getValue() {
    return value;
  }


  /**
   * Creates a new Continent RecordBuilder.
   * @return A new Continent RecordBuilder
   */
  public static n1z.covidcaserecord.Continent.Builder newBuilder() {
    return new n1z.covidcaserecord.Continent.Builder();
  }

  /**
   * Creates a new Continent RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new Continent RecordBuilder
   */
  public static n1z.covidcaserecord.Continent.Builder newBuilder(n1z.covidcaserecord.Continent.Builder other) {
    return new n1z.covidcaserecord.Continent.Builder(other);
  }

  /**
   * Creates a new Continent RecordBuilder by copying an existing Continent instance.
   * @param other The existing instance to copy.
   * @return A new Continent RecordBuilder
   */
  public static n1z.covidcaserecord.Continent.Builder newBuilder(n1z.covidcaserecord.Continent other) {
    return new n1z.covidcaserecord.Continent.Builder(other);
  }

  /**
   * RecordBuilder for Continent instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<Continent>
    implements org.apache.avro.data.RecordBuilder<Continent> {

    private java.lang.String value;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(n1z.covidcaserecord.Continent.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.value)) {
        this.value = data().deepCopy(fields()[0].schema(), other.value);
        fieldSetFlags()[0] = true;
      }
    }

    /**
     * Creates a Builder by copying an existing Continent instance
     * @param other The existing instance to copy.
     */
    private Builder(n1z.covidcaserecord.Continent other) {
            super(SCHEMA$);
      if (isValidValue(fields()[0], other.value)) {
        this.value = data().deepCopy(fields()[0].schema(), other.value);
        fieldSetFlags()[0] = true;
      }
    }

    /**
      * Gets the value of the 'value' field.
      * @return The value.
      */
    public java.lang.String getValue() {
      return value;
    }

    /**
      * Sets the value of the 'value' field.
      * @param value The value of 'value'.
      * @return This builder.
      */
    public n1z.covidcaserecord.Continent.Builder setValue(java.lang.String value) {
      validate(fields()[0], value);
      this.value = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'value' field has been set.
      * @return True if the 'value' field has been set, false otherwise.
      */
    public boolean hasValue() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'value' field.
      * @return This builder.
      */
    public n1z.covidcaserecord.Continent.Builder clearValue() {
      value = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Continent build() {
      try {
        Continent record = new Continent();
        record.value = fieldSetFlags()[0] ? this.value : (java.lang.String) defaultValue(fields()[0]);
        return record;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<Continent>
    WRITER$ = (org.apache.avro.io.DatumWriter<Continent>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<Continent>
    READER$ = (org.apache.avro.io.DatumReader<Continent>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

}
