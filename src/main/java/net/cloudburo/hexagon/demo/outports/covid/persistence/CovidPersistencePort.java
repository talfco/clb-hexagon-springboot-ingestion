package net.cloudburo.hexagon.demo.outports.covid.persistence;

import n1z.covidcaserecord.CovidCaseRecordDomain;
import n1z.covidweeklycaserecord.CovidWeeklyCaseRecordDomain;

public  interface CovidPersistencePort  {

    public void persistDailyCovidRecord(CovidCaseRecordDomain record) throws Exception;
    public void persistWeeklyCovidRecord(CovidWeeklyCaseRecordDomain record) throws Exception;
}
