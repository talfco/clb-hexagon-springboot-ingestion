package net.cloudburo.hexagon.demo.outports.covid.persistence.adapter.sandbox;

import n1z.covidcaserecord.CovidCaseRecordDomain;
import n1z.covidweeklycaserecord.CovidWeeklyCaseRecordDomain;

import net.cloudburo.hexagon.demo.kernel.covid.CovidCaseRecordKernel;
import net.cloudburo.hexagon.demo.kernel.covid.CovidWeeklyCaseRecordKernel;
import net.cloudburo.hexagon.demo.outports.covid.persistence.CovidPersistencePort;
import net.cloudburo.hexagon.demo.outports.covid.persistence.CovidSerializer;

import org.apache.avro.SchemaNormalization;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class CovidSandboxRouter  implements CovidPersistencePort  {

    private static Logger logger = Logger.getLogger(CovidSandboxRouter.class);

    // For illustration purpose we persist the User Data Object as Avro JSON
    private static HashMap<String, String> cache = new HashMap<>();

    @Override
    public void persistDailyCovidRecord(CovidCaseRecordDomain record) throws Exception {
        long fingerprint = SchemaNormalization.parsingFingerprint64(CovidCaseRecordDomain.getClassSchema());
        String id = CovidCaseRecordKernel.getIndex(record);
        logger.info("Added Covid Case record "+id+" to in-memory cache");
        // We inject the Avro fingerprint into the JSON Document, in a sense
        // we seal the document with its specific Schema, this will allow us
        // during retrieval of the document to detect the right schema which
        // can interpret the document (provided by the Schema registry)
        n1z.covidcaserecord.Header header = n1z.covidcaserecord.Header.newBuilder()
                .setAvroFingerprint(fingerprint)
                .setLastUpdateTimestamp(java.lang.System.currentTimeMillis())
                .build();

        CovidCaseRecordDomain updRecord = CovidCaseRecordDomain.newBuilder(record)
        //        .setHeader(header)
                .build();

        String jsonDoc = CovidSerializer.serializeJSON(updRecord);
        logger.info("Adding Daily Record: "+id);
        cache.put(id, jsonDoc);
    }

    @Override
    public void persistWeeklyCovidRecord(CovidWeeklyCaseRecordDomain record) throws Exception {
        long fingerprint = SchemaNormalization.parsingFingerprint64(CovidWeeklyCaseRecordDomain.getClassSchema());
        String id = CovidWeeklyCaseRecordKernel.getIndex(record);
        n1z.covidweeklycaserecord.Header header = n1z.covidweeklycaserecord.Header.newBuilder()
                .setAvroFingerprint(fingerprint)
                .setLastUpdateTimestamp(java.lang.System.currentTimeMillis())
                .build();
        CovidWeeklyCaseRecordDomain updRecord = CovidWeeklyCaseRecordDomain.newBuilder(record)
                .setHeader(header)
                .build();
        String jsonDoc = CovidSerializer.serializeJSON(updRecord);
        logger.info("Adding Weekly Record: "+id);
        cache.put(id, jsonDoc);
    }
}
