package net.cloudburo.hexagon.demo.outports.covid.persistence.adapter.elasticsearch;

import n1z.covidcaserecord.CovidCaseRecordDomain;
import n1z.covidweeklycaserecord.CovidWeeklyCaseRecordDomain;

import net.cloudburo.hexagon.demo.kernel.covid.CovidCaseRecordKernel;
import net.cloudburo.hexagon.demo.kernel.covid.CovidWeeklyCaseRecordKernel;

import net.cloudburo.hexagon.demo.outports.covid.persistence.CovidPersistencePort;
import net.cloudburo.hexagon.demo.outports.covid.persistence.CovidPersistencyPortConfig;
import net.cloudburo.hexagon.demo.outports.covid.persistence.CovidSerializer;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class CovidElasticSearchRouter implements CovidPersistencePort {

    private static Logger logger = Logger.getLogger(CovidElasticSearchRouter.class);

    @Autowired
    private CovidPersistencyPortConfig config;
    @Autowired
    private ESPersistencyManager persistencyManager;

    boolean indexCheck = false;

    private void doIndexCheck() throws IOException {
        if (!indexCheck) {
            if (!persistencyManager.existsIndex(config.getIndexCovid()))
                persistencyManager.createIndex(config.getIndexCovid());
            indexCheck = true;
        }
    }

    protected ESPersistencyManager getPersistencyManager() throws Exception {
        doIndexCheck();
        return this.persistencyManager;
    }

    @Override
    public void persistDailyCovidRecord(CovidCaseRecordDomain record) throws Exception {
        String jsonDoc = CovidSerializer.serializeJSON(record);
        this.getPersistencyManager().createUpdateDocument(config.getIndexCovid(), jsonDoc, CovidCaseRecordKernel.getIndex(record));
    }

    @Override
    public void persistWeeklyCovidRecord(CovidWeeklyCaseRecordDomain record) throws Exception {
        String jsonDoc = CovidSerializer.serializeJSON(record);
        this.getPersistencyManager().createUpdateDocument(config.getIndexCovidWeekly(), jsonDoc, CovidWeeklyCaseRecordKernel.getIndex(record));
    }
}

