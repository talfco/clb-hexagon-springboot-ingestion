package net.cloudburo.hexagon.demo.outports.covid.persistence.adapter.elasticsearch;

public class ESPersistencyException extends Exception {

    int errorCode;

    public ESPersistencyException(int code,String errorMessage) {
        super(errorMessage);
        this.errorCode = code;
    }
    public ESPersistencyException(String errorMessage, Throwable err) {
        super(errorMessage,err);
    }
}
