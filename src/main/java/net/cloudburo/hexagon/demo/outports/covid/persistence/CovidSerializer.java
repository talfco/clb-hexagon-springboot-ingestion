package net.cloudburo.hexagon.demo.outports.covid.persistence;


import n1z.covidcaserecord.CovidCaseRecordDomain;
import n1z.covidweeklycaserecord.CovidWeeklyCaseRecordDomain;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.io.*;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;

import java.io.ByteArrayOutputStream;

public class CovidSerializer {

    public static String serializeJSON(CovidCaseRecordDomain request) throws Exception {
        DatumWriter<CovidCaseRecordDomain> writer = new SpecificDatumWriter<CovidCaseRecordDomain>(CovidCaseRecordDomain.class);
        byte[] data = new byte[0];
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Encoder jsonEncoder = null;
        jsonEncoder = EncoderFactory.get().jsonEncoder(CovidCaseRecordDomain.getClassSchema(), stream);
        writer.write(request, jsonEncoder);
        jsonEncoder.flush();
        data = stream.toByteArray();
        return new String(data);
    }



    public static String serializeJSON(CovidWeeklyCaseRecordDomain request) throws Exception {
        DatumWriter<CovidWeeklyCaseRecordDomain> writer = new SpecificDatumWriter<CovidWeeklyCaseRecordDomain>(CovidWeeklyCaseRecordDomain.class);
        byte[] data = new byte[0];
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Encoder jsonEncoder = null;
        jsonEncoder = EncoderFactory.get().jsonEncoder(CovidWeeklyCaseRecordDomain.getClassSchema(), stream);
        writer.write(request, jsonEncoder);
        jsonEncoder.flush();
        data = stream.toByteArray();
        return new String(data);
    }

    public static CovidCaseRecordDomain deSerializeCovidJSON(String data) throws Exception {
        DatumReader<CovidCaseRecordDomain> reader = new SpecificDatumReader<>(CovidCaseRecordDomain.class);
        Decoder decoder = DecoderFactory.get().jsonDecoder(CovidCaseRecordDomain.getClassSchema(),data);
        return reader.read(null, decoder);
    }

    // TODO this is untested
    public CovidCaseRecordDomain schemaEvolutionCovidJSON(String data, Schema from, Schema to) throws Exception {
        DatumReader<CovidCaseRecordDomain> reader = new GenericDatumReader<>(from,to);
        Decoder decoder = DecoderFactory.get().jsonDecoder(to,data);
        return reader.read(null, decoder);
    }
}
