package net.cloudburo.hexagon.demo.kernel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;

@Component
public class GeneratorHelper {

    private static final Logger logger = LoggerFactory.getLogger(GeneratorHelper.class);


    public static long getMillisFromDateTime(LocalDateTime localDateTime) {
        //return localDateTime.ZoneId.systemDefault().toEpochMilli();
        /*
        System.out.println("Input: 9999-12-31-00.00.00.000000 with formatter 'yyyy-MM-dd-HH.mm.ss.SSSSSS'");
        LocalDateTime dt = LocalDateTime.parse("9999-12-31-00.00.00.000000", DateTimeFormatter.ofPattern("yyyy-MM-dd-HH.mm.ss.SSSSSS"));
        System.out.println("Local Time"+dt.toString());
        ZonedDateTime zt = ZonedDateTime.of(dt, ZoneId.of("UTC"));
        System.out.println("Zonetime Epoch (Milliseconds): "+ zt.toInstant().toEpochMilli());
        System.out.println("ZoneDateTime UTC:"+zt.toString());
        System.out.println("ZoneDateTime UTC ISO8601:"+zt.toOffsetDateTime().toString());

        zt = ZonedDateTime.of(dt, ZoneId.of("Europe/Zurich"));
        System.out.println("ZoneDateTime  MEZ  Epoch (Milliseconds): "+ zt.toInstant().toEpochMilli());
        System.out.println("ZoneDatime MEZ:"+zt.toString());
        System.out.println("ZoneDateTime MEZ ISO8601:"+zt.toOffsetDateTime().toString()); */
        return ZonedDateTime.of(localDateTime, ZoneId.systemDefault()).toInstant().toEpochMilli();

    }

    public static long getMillisFromDateTimeTZ(LocalDateTime localDateTime, String zoneId) {
        ZonedDateTime zt = ZonedDateTime.of(localDateTime, ZoneId.of(zoneId));
        return zt.toInstant().toEpochMilli();
    }

    public static String getMicroFromStringTZ(LocalDateTime localDateTime, String zoneId) {
        ZonedDateTime zt = ZonedDateTime.of(localDateTime, ZoneId.of(zoneId));
        return zt.toOffsetDateTime().toString();
    }

    public static String getMicroFromString(LocalDateTime localDateTime) {
        ZonedDateTime zt = ZonedDateTime.of(localDateTime,  ZoneId.systemDefault());
        return zt.toOffsetDateTime().toString();
    }

    public static LocalDate fixDateHL(String data, String pattern) {
        if (data.contains("0000")||data.contains("0001")) {
            return null;
        }
        return LocalDate.from(DateTimeFormatter.ofPattern(pattern).parse(data));
    }

    public static ProcessingCenterObject getProcessingCenter(String  key,ProcessingCenterConfig processingCenterConfig) throws Exception {
        Iterator<ProcessingCenterObject> it = processingCenterConfig.getPc().iterator();
        while (it.hasNext()) {
            ProcessingCenterObject obj = it.next();
            if (obj.getPckey().equals(key)) return obj;
        }
        String msg ="Can't find Processing Center Entry for Key "+key;
        logger.error(msg);
        throw new Exception(msg);
    }

}
