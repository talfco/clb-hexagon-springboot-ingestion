package net.cloudburo.hexagon.demo.kernel;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties("injector.processingcenter")
public class ProcessingCenterConfig {

    List<ProcessingCenterObject> pc;

    public List<ProcessingCenterObject> getPc() {
        return pc;
    }

    public void setPc(List<ProcessingCenterObject> processingCenterList) {
        this.pc = processingCenterList;
    }
}
