package net.cloudburo.hexagon.demo.kernel;

public class ProcessingCenterObject {

    private String pckey;
    private String pcId;
    private String tzZoneId;
    private String tzCountryCode;


    public String getPckey() {
        return pckey;
    }

    public void setPckey(String pckey) {
        this.pckey = pckey;
    }

    public String getPcId() {
        return pcId;
    }

    public void setPcId(String pcId) {
        this.pcId = pcId;
    }

    public String getTzZoneId() {
        return tzZoneId;
    }

    public void setTzZoneId(String tzZoneId) {
        this.tzZoneId = tzZoneId;
    }

    public String getTzCountryCode() {
        return tzCountryCode;
    }

    public void setTzCountryCode(String tzCountryCode) {
        this.tzCountryCode = tzCountryCode;
    }
}
