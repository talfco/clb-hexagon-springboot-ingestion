package net.cloudburo.hexagon.demo.inports.covid.staging;


import n1z.covidcaserecord.CovidCaseRecordDomain;
import n1z.covidweeklycaserecord.CovidWeeklyCaseRecordDomain;

/*
A port interfaces understands the domain model data structure, which are passed in or retrieved
In our case Avro Schema based data structures.
The port will be implemented by a corresponding UseCaseRepository in the kernel.usecase package.
The adapter implementation will be wired with UseCaseRepository
 */
public interface CovidStagingPort {

    public void addDailyCovidCases(CovidCaseRecordDomain caseRecord) throws Exception;

    public void addWeeklyCovidCases(CovidWeeklyCaseRecordDomain caseRecord) throws Exception;
}

