package net.cloudburo.hexagon.demo.inports.covid.staging;


import net.cloudburo.hexagon.demo.inports.ProcessingCenterRefObject;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties("injector.staging.covid")
public class CovidStagingPortConfig {

    private String source;

    private String target;

    private String deadletter;

    private List<ProcessingCenterRefObject> processingCenterList;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getDeadletter() {
        return deadletter;
    }

    public void setDeadletter(String deadletter) {
        this.deadletter = deadletter;
    }

    public List<ProcessingCenterRefObject> getProcessingCenterList() {
        return processingCenterList;
    }

    public void setProcessingCenterList(List<ProcessingCenterRefObject> processingCenterList) {
        this.processingCenterList = processingCenterList;
    }
}
