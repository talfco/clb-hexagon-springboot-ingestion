package net.cloudburo.hexagon.demo.generator;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

import java.util.HashMap;
import java.util.Iterator;

import static net.cloudburo.hexagon.demo.generator.Constants.GEN_CFG;

public class KernelClassGeneratorAggregationStrategy extends GeneratorAggregationBase implements AggregationStrategy {

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        BindyInterfaceSpec rec = (BindyInterfaceSpec) newExchange.getIn().getBody();
        GeneratorDomainObject cfg = (GeneratorDomainObject)newExchange.getIn().getHeader(GEN_CFG);
        String className = cfg.getClassName();
        StringBuffer genContent;
        if (oldExchange == null)  {
            genContent = new StringBuffer();
            attrList = new HashMap<String,BindyInterfaceSpec>();
            String attrName = getAttributeName(rec,false);
            attrList.put(attrName, rec);
            newExchange.getIn().setBody(genContent);
            return newExchange;
        } else {
            genContent = oldExchange.getIn().getBody(StringBuffer.class);
            String attrName = getAttributeName(rec,false);
            attrList.put(attrName, rec);
            oldExchange.setProperty("CamelSplitComplete", newExchange.getProperty("CamelSplitComplete"));
            if (newExchange.getProperty("CamelSplitComplete",Boolean.class)){
                generateGetIndexMethod(genContent, className);
            }
            return oldExchange;
        }
    }

    private void generateGetIndexMethod(StringBuffer buf, String className) {
        Iterator<String> attributeIt = attrList.keySet().iterator();
        buf.append("\n  public static String getIndex(");
        buf.append(className+"Domain record");
        buf.append(") {\n");
        buf.append("      String id = ");
        boolean first = true;
        while (attributeIt.hasNext()) {
            String clzName = attributeIt.next();
            String attrName = clzName.substring(0, 1).toLowerCase() + clzName.substring(1);
            if (attrList.get(clzName).getTargetRelev().equals(Constants.TARGET_RELEV_KE)) {
                if (!first) buf.append(" + ");
                buf.append("record.getKey().get"+clzName+"().getValue()");
                first = false;
            }
        }
        buf.append(";\n");
        buf.append("      id = id.replaceAll(\"\\\\s+\",\"\");\n");
        buf.append("      return id;\n");
        buf.append("  }\n");
    }
}
