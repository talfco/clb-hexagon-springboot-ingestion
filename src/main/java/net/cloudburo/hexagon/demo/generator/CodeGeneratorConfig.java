package net.cloudburo.hexagon.demo.generator;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties("injector.generator")
public class CodeGeneratorConfig {

    List<GeneratorDomainObject> domain;

    public List<GeneratorDomainObject> getDomain() {
        return domain;
    }

    public void setDomain(List<GeneratorDomainObject> domain) {
        this.domain = domain;
    }
}
