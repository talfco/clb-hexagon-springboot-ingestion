package net.cloudburo.hexagon.demo.generator;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.StringWriter;
import java.util.*;

import static net.cloudburo.hexagon.demo.generator.Constants.GEN_CFG;

public class AvroSchemaGeneratorAggregationStrategy  implements AggregationStrategy {

    private static final Logger logger = LoggerFactory.getLogger(AvroSchemaGeneratorAggregationStrategy.class);

    JsonGenerator avroSchemaJSONgenerator = null;
    HashMap<String,BindyInterfaceSpec> attributeList = null;

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        // Here we keep a reference, so we don't have explicitly set the body in the else-clause
        BindyInterfaceSpec rec = (BindyInterfaceSpec) newExchange.getIn().getBody();
        GeneratorDomainObject cfg = (GeneratorDomainObject)newExchange.getIn().getHeader(GEN_CFG);
        String namespace = cfg.getAvroNamespace();
        String className = cfg.getClassName();

        if (oldExchange == null) {
            try {
                JsonFactory factory = new JsonFactory();
                StringWriter jsonObjectWriter = new StringWriter();
                attributeList = new HashMap<>();
                avroSchemaJSONgenerator = factory.createJsonGenerator(jsonObjectWriter);
                avroSchemaJSONgenerator.useDefaultPrettyPrinter();
                avroSchemaJSONgenerator.writeStartArray();
                generateHeader(avroSchemaJSONgenerator, namespace);
                generateSimpleTypeForAvro(avroSchemaJSONgenerator,rec,newExchange,namespace);
                attributeList.put(getAttributeName(rec),rec);
                newExchange.getIn().setBody(avroSchemaJSONgenerator,JsonGenerator.class);
            } catch (IOException ex) {
                newExchange.setException(ex);
            }
            return newExchange;
        } else {
            try {
                generateSimpleTypeForAvro(avroSchemaJSONgenerator,rec,newExchange,namespace);
                attributeList.put(getAttributeName(rec),rec);
                if (newExchange.getProperty("CamelSplitComplete",Boolean.class)){
                    generatePrimaryKey(avroSchemaJSONgenerator,namespace);
                    generateDatasetContainer(avroSchemaJSONgenerator, namespace,className+"Domain",attributeList);
                    avroSchemaJSONgenerator.writeEndArray();
                    avroSchemaJSONgenerator.close();
                }
                oldExchange.setProperty("CamelSplitComplete", newExchange.getProperty("CamelSplitComplete"));
            } catch (IOException ex) {
                oldExchange.setException(ex);
            }
            return oldExchange;
        }
    }

    private static String getAvroTypeName(String name) {
        return name.substring(0,1).toUpperCase() + name.substring(1);//.toLowerCase();
    }

    private void generatePrimaryKey(JsonGenerator generator, String namespace) throws IOException{

        generator.writeStartObject();
        generator.writeFieldName("type");
        generator.writeString("record");
        generator.writeFieldName("namespace");
        generator.writeString(namespace);
        generator.writeFieldName("name");
        generator.writeString("PrimaryKey");
        generator.writeFieldName("doc");
        generator.writeString("The primary keys elements of the Avro Domain Object Schema");
        generator.writeFieldName("fields");
        generator.writeStartArray();

        for (int i=0; i< attributeList.values().size(); i++) {
            BindyInterfaceSpec rec = (BindyInterfaceSpec) attributeList.values().toArray()[i];
            if (rec.getTargetRelev().equals(Constants.TARGET_RELEV_KE)) {
                String attributeName = (String)attributeList.keySet().toArray()[i];
                if (rec.getTargetName() != null && !rec.getTargetName().equals(""))
                    attributeName = rec.getTargetName();
                generator.writeStartObject();
                generator.writeFieldName("name");
                generator.writeString(attributeName.substring(0, 1).toLowerCase() + attributeName.substring(1));
                generator.writeFieldName("type");
                generator.writeString(attributeName.substring(0, 1).toUpperCase() + attributeName.substring(1));
                generator.writeEndObject();
            }
        }

        generator.writeEndArray();
        generator.writeEndObject();

    }

    private void generateHeader(JsonGenerator generator, String namespace) throws IOException{
        // Header
        generator.writeStartObject();
        generator.writeFieldName("type");
        generator.writeString("record");
        generator.writeFieldName("namespace");
        generator.writeString(namespace);
        generator.writeFieldName("name");
        generator.writeString("Header");
        generator.writeFieldName("doc");
        generator.writeString("The common header of Avro Domain Object Schema");
        generator.writeFieldName("fields");
        generator.writeStartArray();;

        generator.writeStartObject();
        generator.writeFieldName("name");
        generator.writeString("avroFingerprint");
        generator.writeFieldName("type");
        generator.writeString("long");
        generator.writeEndObject();

        generator.writeStartObject();
        generator.writeFieldName("name");
        generator.writeString("lastUpdateLoginId");
        generator.writeFieldName("type");
        generator.writeStartArray();
        generator.writeString("null");
        generator.writeString("string");
        generator.writeEndArray();
        generator.writeFieldName("default");
        generator.writeNull();
        generator.writeEndObject();

        generator.writeStartObject();
        generator.writeFieldName("name");
        generator.writeString("lastUpdateTimestamp");
        generator.writeFieldName("type");
        generator.writeStartArray();
        generator.writeString("null");
        generator.writeString("long");
        generator.writeEndArray();
        generator.writeFieldName("default");
        generator.writeNull();
        generator.writeFieldName("logicalType");
        generator.writeString("timestamp-micros");
        generator.writeEndObject();

        generator.writeStartObject();
        generator.writeFieldName("name");
        generator.writeString("ordsUpdateTimestamp");
        generator.writeFieldName("type");
        generator.writeStartArray();
        generator.writeString("null");
        generator.writeString("long");
        generator.writeEndArray();
        generator.writeFieldName("default");
        generator.writeNull();
        generator.writeFieldName("logicalType");
        generator.writeString("timestamp-micros");
        generator.writeEndObject();

        generator.writeStartObject();
        generator.writeFieldName("name");
        generator.writeString("businessDay");
        generator.writeFieldName("type");
        generator.writeStartArray();
        generator.writeString("null");
        generator.writeString("int");
        generator.writeEndArray();
        generator.writeFieldName("default");
        generator.writeNull();
        generator.writeFieldName("logicalType");
        generator.writeString("date");
        generator.writeEndObject();

        generator.writeStartObject();
        generator.writeFieldName("name");
        generator.writeString("processingCenterID");
        generator.writeFieldName("type");
        generator.writeStartArray();
        generator.writeString("null");
        generator.writeString("string");
        generator.writeEndArray();
        generator.writeFieldName("default");
        generator.writeNull();
        generator.writeFieldName("doc");
        generator.writeString("identifier of a processing center (mandator)");
        generator.writeEndObject();

        generator.writeStartObject();
        generator.writeFieldName("name");
        generator.writeString("processingCenterTZ");
        generator.writeFieldName("type");
        generator.writeStartArray();
        generator.writeString("null");
        generator.writeString("string");
        generator.writeEndArray();
        generator.writeFieldName("default");
        generator.writeNull();
        generator.writeFieldName("doc");
        generator.writeString("time zone as defined in java.time.ZoneId");
        generator.writeEndObject();

        generator.writeStartObject();
        generator.writeFieldName("name");
        generator.writeString("processingCenterAlpha2TZ");
        generator.writeFieldName("type");
        generator.writeStartArray();
        generator.writeString("null");
        generator.writeString("string");
        generator.writeEndArray();
        generator.writeFieldName("default");
        generator.writeNull();
        generator.writeFieldName("doc");
        generator.writeString("time tone as defined in java Country Code");
        generator.writeEndObject();

        generator.writeEndArray();
        generator.writeEndObject();
    }

    private String createDoc(BindyInterfaceSpec rec) {
        if ( rec.getLength()>0) {
            return "Length: "  + rec.getLength();
        }
        return "";
    }

    private String getAttributeName(BindyInterfaceSpec rec) {
        if (rec.getTargetName()!=null&&!rec.getTargetName().equals(""))
            return rec.getTargetName();
        else
            return rec.getAttributeName();
    }

    private void generateSimpleTypeForAvro(JsonGenerator generator, BindyInterfaceSpec rec, Exchange exchange,String namespace) throws IOException {
        if (!rec.getTargetRelev().equals(Constants.TARGET_RELEV_PL)  && !rec.getTargetRelev().equals (Constants.TARGET_RELEV_KE)) return;
        switch(rec.getType()) {
            case "Boolean":
                writeSimpleTypeForAvro(avroSchemaJSONgenerator, namespace, getAttributeName(rec), "boolean", "Length: " + createDoc(rec), null,rec.isOptional(), rec.getDefaultValue());
                break;
            case "Integer":
                writeSimpleTypeForAvro(avroSchemaJSONgenerator, namespace, getAttributeName(rec), "int", "Length: " + createDoc(rec), null,rec.isOptional(), rec.getDefaultValue());
                break;
            case "Long":
                writeSimpleTypeForAvro(avroSchemaJSONgenerator, namespace, getAttributeName(rec), "long", "Length: " + createDoc(rec), null,rec.isOptional(), rec.getDefaultValue());
                break;
            case "Float":
                writeSimpleTypeForAvro(avroSchemaJSONgenerator, namespace, getAttributeName(rec), "float", "Length: " + createDoc(rec), null,rec.isOptional(), rec.getDefaultValue());
                break;
            case "Double":
                writeSimpleTypeForAvro(avroSchemaJSONgenerator, namespace, getAttributeName(rec), "double", "Length: " + createDoc(rec), null,rec.isOptional(), rec.getDefaultValue());
                break;
            case "String":
                writeSimpleTypeForAvro(avroSchemaJSONgenerator, namespace, getAttributeName(rec), "string", "Length: " + createDoc(rec), null,rec.isOptional(), rec.getDefaultValue());
                break;
            case "Date":
                writeSimpleTypeForAvro(avroSchemaJSONgenerator, namespace, getAttributeName(rec), "int", "Length: " + createDoc(rec), "date",rec.isOptional(), rec.getDefaultValue());
                break;
            case "DateFixHL":
                writeSimpleTypeForAvro(avroSchemaJSONgenerator, namespace, getAttributeName(rec), "int", "Length: " + createDoc(rec), "time-millis",rec.isOptional(), rec.getDefaultValue());
                break;
            case "TimeMillis":
                writeSimpleTypeForAvro(avroSchemaJSONgenerator, namespace, getAttributeName(rec), "int", "Length: " + createDoc(rec), "time-millis",rec.isOptional(), rec.getDefaultValue());
                break;
            case "TimeMicros":
                writeSimpleTypeForAvro(avroSchemaJSONgenerator, namespace, getAttributeName(rec), "long", "Length: " + createDoc(rec), "time-micros",rec.isOptional(), rec.getDefaultValue());
                break;
            case "TimestampMillis":
                writeSimpleTypeForAvro(avroSchemaJSONgenerator, namespace, getAttributeName(rec), "long", "Length: " + createDoc(rec), "timestamp-millis",rec.isOptional(), rec.getDefaultValue());
                break;
            case "LocalTimestampMillis":
                writeSimpleTypeForAvro(avroSchemaJSONgenerator, namespace, getAttributeName(rec), "long", "Length: " + createDoc(rec), "local-timestamp-millis",rec.isOptional(), rec.getDefaultValue());
                break;
            case "TimestampMicros":
                writeSimpleTypeForAvro(avroSchemaJSONgenerator, namespace, getAttributeName(rec), "long", "Length: " + createDoc(rec), "timestamp-micros",rec.isOptional(), rec.getDefaultValue());
                break;
            case "LocalTimestampMicros":
                writeSimpleTypeForAvro(avroSchemaJSONgenerator, namespace, getAttributeName(rec)+"Millis", "long", "Length: " + createDoc(rec), "local-timestamp-micros",rec.isOptional(), rec.getDefaultValue());
                writeSimpleTypeForAvro(avroSchemaJSONgenerator, namespace, getAttributeName(rec), "string", "Length: " + createDoc(rec), null,rec.isOptional(), rec.getDefaultValue());
                attributeList.put(getAttributeName(rec)+"Millis",rec);
                break;
            case "LocalTimestampMicrosFixLowValue":
                writeSimpleTypeForAvro(avroSchemaJSONgenerator, namespace, getAttributeName(rec)+"Millis", "long", "Length: " + createDoc(rec), "local-timestamp-micros",rec.isOptional(), rec.getDefaultValue());
                writeSimpleTypeForAvro(avroSchemaJSONgenerator, namespace, getAttributeName(rec), "string", "Length: " + createDoc(rec), null,true, rec.getDefaultValue());
                attributeList.put(getAttributeName(rec)+"Millis",rec);
                break;
            case "BigDecimal":
                writeSimpleTypeForAvro(avroSchemaJSONgenerator, namespace, getAttributeName(rec), "float", "Length: " + createDoc(rec) + " precision: " + rec.getPrecision(), "float",rec.isOptional(), rec.getDefaultValue());
                break;
            default:
                Exception ex = new Exception("No mapping found for type " + rec.getType());
                exchange.setException(ex);
        }

    }

    private void writeSimpleTypeForAvro(JsonGenerator generator, String namespace, String name, String type, String doc,
                                        String logicalType, boolean optional, String defaultValue) throws IOException {
        String nameC = getAvroTypeName(name);
        generator.writeStartObject();
        generator.writeFieldName("type");
        generator.writeString("record");
        generator.writeFieldName("namespace");
        generator.writeString(namespace);
        generator.writeFieldName("name");
        generator.writeString(nameC);
        generator.writeFieldName("doc");
        generator.writeString(doc);
        generator.writeFieldName("fields");
        generator.writeStartArray();;
        generator.writeStartObject();
        generator.writeFieldName("name");
        generator.writeString("value");
        if (!optional) {
            generator.writeFieldName("type");
            generator.writeString(type);
            if (defaultValue != null && !defaultValue.equals("")) {
                generator.writeFieldName("default");
                generator.writeString(defaultValue);
            }
        } else
        {
            generator.writeFieldName("type");
            generator.writeStartArray();
            generator.writeString("null");
            generator.writeString(type);
            generator.writeEndArray();
            generator.writeFieldName("default");
            generator.writeNull();
        }

        if (logicalType != null) {
            generator.writeFieldName("logicalType");
            generator.writeString(logicalType);
        }
        generator.writeEndObject();
        generator.writeEndArray();
        generator.writeEndObject();
    }

    private void generateDatasetContainer(JsonGenerator generator, String namespace, String complexTypeName, HashMap<String, BindyInterfaceSpec> list) throws IOException {
        generator.writeStartObject();
        generator.writeFieldName("type");
        generator.writeString("record");
        generator.writeFieldName("namespace");
        generator.writeString(namespace);
        generator.writeFieldName("name");
        generator.writeString(complexTypeName);
        generator.writeFieldName("doc");
        generator.writeString("Domain Record ");
        generator.writeFieldName("fields");
        generator.writeStartArray();
        // Header
        generator.writeStartObject();
        generator.writeFieldName("name");
        generator.writeString("header");
        generator.writeFieldName("type");
        generator.writeString("Header");
        generator.writeEndObject();
        //  Primary Key
        generator.writeStartObject();
        generator.writeFieldName("name");
        generator.writeString("key");
        generator.writeFieldName("type");
        generator.writeString("PrimaryKey");
        generator.writeEndObject();
        // Payload Attributes
        for (int i=0; i< list.values().size(); i++) {
            BindyInterfaceSpec rec = (BindyInterfaceSpec) list.values().toArray()[i];
            if (rec.getTargetRelev().equals(Constants.TARGET_RELEV_PL)) {
                String attributeName = (String)list.keySet().toArray()[i];
                if (rec.getTargetName() != null && !rec.getTargetName().equals(""))
                    attributeName = rec.getTargetName();
                generator.writeStartObject();
                generator.writeFieldName("name");
                generator.writeString(attributeName.substring(0, 1).toLowerCase() + attributeName.substring(1));
                generator.writeFieldName("type");
                generator.writeString(attributeName.substring(0, 1).toUpperCase() + attributeName.substring(1));
                generator.writeEndObject();

            }
        }
        generator.writeEndArray();
        generator.writeEndObject();
    }
}
