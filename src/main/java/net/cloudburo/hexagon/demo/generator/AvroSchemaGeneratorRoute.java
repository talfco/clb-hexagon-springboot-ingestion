package net.cloudburo.hexagon.demo.generator;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.BindyType;
import org.codehaus.jackson.JsonGenerator;
import org.springframework.stereotype.Component;

import java.io.StringWriter;

import static net.cloudburo.hexagon.demo.generator.Constants.GEN_CFG;

@Component
public class AvroSchemaGeneratorRoute extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        // Avro Schema Generator Part
        from("direct://generateAvroSchema").routeId("avro-schema-generator-route")
            .split().tokenize("\n", 1, true).streaming()
            .unmarshal()
            .bindy(BindyType.Csv, BindyInterfaceSpec.class)
            .process(new Processor() {
                public void process(Exchange exchange) throws Exception {
                    GeneratorDomainObject cfg = (GeneratorDomainObject)exchange.getIn().getHeader(GEN_CFG);
                    exchange.getIn().setHeader(Exchange.FILE_NAME, cfg.getClassName()+"Domain.avsc");
                }
            })
            .aggregate(constant(true),new AvroSchemaGeneratorAggregationStrategy())
            .completionPredicate(simple("${property.CamelSplitComplete} == true"))
            .process(new Processor(){
                public void process(Exchange exchange) throws Exception {
                    JsonGenerator generator = exchange.getIn().getBody(JsonGenerator.class);
                    String content = ((StringWriter)generator.getOutputTarget()).toString();
                    exchange.getIn().setBody(content);
                }
            })
            .to("file://generated")
            .log("Code Generator - Avro Schema completed");
    }
}
