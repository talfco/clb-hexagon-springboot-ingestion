package net.cloudburo.hexagon.demo.generator;

public class Constants {

    public static final String TARGET_RELEV_PL = "PL";
    public static final String TARGET_RELEV_HE = "HE";
    public static final String TARGET_RELEV_KE = "KE";

    final public static String FIXED_LEN="fixedlen";
    public static final String GEN_FILE_LENGTH = "fileLength";
    public static final String GEN_CFG = "genConfigObject";
}
