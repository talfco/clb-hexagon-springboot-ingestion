package net.cloudburo.hexagon.demo.generator;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

@CsvRecord( separator = "," )
public class BindyInterfaceSpec {

    @DataField(pos = 1)
    private int position;

    @DataField(pos = 2)
    private String attributeName;

    @DataField(pos=3)
    private int length;

    @DataField(pos = 4)
    private boolean cid;

    @DataField(pos = 5)
    private String type;
    
    @DataField(pos = 6)
    private String pattern;

    @DataField(pos = 7)
    private boolean trim;

    @DataField(pos = 8)
    private boolean alignLeft;

    @DataField(pos = 9)
    private String groupingSep;

    @DataField(pos = 10)
    private int precision;

    @DataField(pos =11)  // HE, PL, null
    private String targetRelev;

    @DataField(pos =12)
    private String targetName;

    @DataField(pos =13)
    private boolean optional;

    @DataField(pos =14)
    private String defaultValue;

    @DataField (pos=15)
    private String remark;

    public int getPosition() {
        return position;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public int getLength() {
        return length;
    }

    public boolean isCid() {
        return cid;
    }

    public String getType() {
        return type;
    }

    public String getPattern() {
        return pattern;
    }

    public boolean isTrim() {
        return trim;
    }

    public boolean isAlignLeft() {
        return alignLeft;
    }

    public String getGroupingSep() {
        return groupingSep;
    }

    public int getPrecision() {
        return precision;
    }

    public String getTargetRelev() {
        return targetRelev;
    }

    public String getTargetName() {
        return targetName;
    }

    public String getRemark() {
        return remark;
    }

    public boolean isOptional() {
        return optional;
    }

    public String getDefaultValue() {
        return defaultValue;
    }
}
