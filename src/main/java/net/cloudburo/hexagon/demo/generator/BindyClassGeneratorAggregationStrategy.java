package net.cloudburo.hexagon.demo.generator;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Iterator;

import static net.cloudburo.hexagon.demo.generator.Constants.FIXED_LEN;
import static net.cloudburo.hexagon.demo.generator.Constants.GEN_CFG;
import static net.cloudburo.hexagon.demo.generator.Constants.GEN_FILE_LENGTH;

public class BindyClassGeneratorAggregationStrategy extends GeneratorAggregationBase implements AggregationStrategy {

    private static final Logger logger = LoggerFactory.getLogger(BindyClassGeneratorAggregationStrategy.class);

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        // Here we keep a reference, so we don't have explicityl set the body in the else-clause
        BindyInterfaceSpec rec = (BindyInterfaceSpec) newExchange.getIn().getBody();
        GeneratorDomainObject cfg = (GeneratorDomainObject)newExchange.getIn().getHeader(GEN_CFG);
        String className = cfg.getClassName();
        int filelength =1;
        StringBuffer genContent;
        if (oldExchange == null)  {
            filelength += rec.getLength();
            genContent = new StringBuffer();
            attrList = new HashMap<String,BindyInterfaceSpec>();
            String attrName = getAttributeName(rec,false);
            attrList.put(attrName, rec);
            generateFileCodePart(genContent, rec,cfg);
            newExchange.getIn().setBody(genContent);
            newExchange.getIn().setHeader(GEN_FILE_LENGTH, filelength);
            return newExchange;
        } else {
            genContent = oldExchange.getIn().getBody(StringBuffer.class);
            filelength = oldExchange.getIn().getHeader(GEN_FILE_LENGTH, Integer.class);
            String attrName = getAttributeName(rec,false);
            attrList.put(attrName, rec);
            generateFileCodePart(genContent,rec,cfg);
            filelength += rec.getLength();
            oldExchange.getIn().setHeader(GEN_FILE_LENGTH,filelength);
            oldExchange.setProperty("CamelSplitComplete", newExchange.getProperty("CamelSplitComplete"));
            if (newExchange.getProperty("CamelSplitComplete",Boolean.class)){
                generateDomainMethod(genContent,className);
            }
            return oldExchange;
        }
    }
    
    private void generateFileCodePart(StringBuffer genContent, BindyInterfaceSpec rec, GeneratorDomainObject cfg ) {
        genContent.append("\n  @DataField(pos = "+rec.getPosition());
        if (cfg.getFileFormat().equals(FIXED_LEN)) {
            genContent.append(", length=" + rec.getLength());
            if (rec.isTrim()) {
                genContent.append(", trim = true");
            }
            if (rec.isAlignLeft()) {
                genContent.append((", align = \"L\""));
            }
        }
        if (rec.getPattern() !=null && !rec.getPattern().equals("") && !rec.getType().equals("DateFixHL")) {
            genContent.append(", pattern = \"" + rec.getPattern()+"\"");
        }
        if (rec.getGroupingSep() != null && !rec.getGroupingSep().equals("")) {
            genContent.append((", groupingSeparator = \"")+rec.getGroupingSep()+"\"");
            genContent.append(", precision = "+rec.getPrecision());
        }
        genContent.append(")\n");
        String attrName = getAttributeName(rec,true);
        String type = rec.getType();
        if (type.equals("Date"))
            type = "LocalDate";
        else if (type.equals("DateFixHL"))
            type = "String";
        else if (type.equals("TimeMillis"))
            type = "LocalTime";
        else if (type.equals("TimeMicros"))
            type = "LocalTime";
        else if (type.equals("TimestampMillis"))
            type = "LocalDateTime";
        else if (type.equals("TimestampMicros"))
            type = "LocalDateTime";
        else if (type.equals("LocalTimestampMillis"))
            type = "LocalDateTime";
        else if (type.equals("LocalTimestampMicros"))
            type = "LocalDateTime";
        else if (type.equals("LocalTimestampMicrosFixLowValue"))
            type = "String";
        genContent.append("  private "+type+" "+attrName+"; ");
        genContent.append(" // CID: "+rec.isCid()+" \n");
    }

    private void generateDomainMethod(StringBuffer buf, String className) {
        buf.append("\n  public "+className+"Domain transformToDomainClass(String zoneId, String zoneCountryCode) {\n");
        generateBuilderInitializer(buf,className,Constants.TARGET_RELEV_HE);
        generateBuilderInitializer(buf,className,Constants.TARGET_RELEV_KE);
        generateBuilderInitializer(buf,className,Constants.TARGET_RELEV_PL);
        buf.append("    "+className+"Domain "+className.toLowerCase()+" = "+className+"Domain.newBuilder()");
        generateAttributeSetting(buf,className,Constants.TARGET_RELEV_HE, false);
        generateAttributeSetting(buf,className,Constants.TARGET_RELEV_KE, false);
        generateAttributeSetting(buf,className,Constants.TARGET_RELEV_PL, true);
        // KE Part has no optional
        buf.append(("   Header header = "+className.toLowerCase()+".getHeader();\n"));
        generateOptionalAttributeInit(buf,className,Constants.TARGET_RELEV_HE);
        generateOptionalAttributeInit(buf,className,Constants.TARGET_RELEV_PL);
        //generateHeaderCode(buf, className);
        buf.append("\n    return "+className.toLowerCase()+";\n");
        buf.append("  \n}\n");
    }

    private void generateBuilderInitializer(StringBuffer buf, String className, String targetRelevancy) {
        Iterator<String> attributeIt = attrList.keySet().iterator();
        while (attributeIt.hasNext()) {
            String attrName = attributeIt.next();
            String var = attrName.substring(0,1).toLowerCase() + attrName.substring(1);
            if (attrList.get(attrName).getTargetRelev().equals(targetRelevancy)) {
                switch (attrList.get(attrName).getType()) {
                    case "BigDecimal":
                        buf.append("\n");
                        buf.append("    float " + var + " = 0;\n");
                        buf.append("    if (this." + var + " != null) " + var + " = this." + var + ".floatValue();\n");
                        break;
                    case "DateFixHL":
                        buf.append("    LocalDate " + var + " = GeneratorHelper.fixDateHL(this." + var + "\"" + attrList.get(attrName).getPattern() + "\");\n");
                        break;
                }
            }
        }


    }

    private void generateAttributeSetting(StringBuffer buf, String className, String targetRelevancy, boolean buildCompletion) {
        String spacer = "      ";
        if (targetRelevancy.equals(Constants.TARGET_RELEV_KE)) {
            buf.append("\n"+spacer+".setKey(PrimaryKey.newBuilder()");
            spacer = "        ";
        } else if (targetRelevancy.equals(Constants.TARGET_RELEV_HE)) {
            buf.append("\n"+spacer+".setHeader(Header.newBuilder()");
            spacer = "         ";
            buf.append("\n"+spacer+".setAvroFingerprint(SchemaNormalization.parsingFingerprint64("+className+"Domain.getClassSchema()))");
            buf.append("\n"+spacer+".setOrdsUpdateTimestamp(java.lang.System.currentTimeMillis())");
            buf.append("\n"+spacer+".setProcessingCenterTZ(zoneId)");
            buf.append("\n"+spacer+".setProcessingCenterAlpha2TZ(zoneCountryCode)");
        }

        Iterator<String> attributeIt = attrList.keySet().iterator();
        while (attributeIt.hasNext()) {
            String clzName = attributeIt.next();
            String attrName = clzName.substring(0,1).toLowerCase() + clzName.substring(1);
            String typeName = attrList.get(clzName).getType();
            String pattern = attrList.get(clzName).getPattern();
            if (attrList.get(clzName).getTargetRelev()!=null && attrList.get(clzName).getTargetRelev().equals(targetRelevancy)) {
                if (!attrList.get(clzName).isOptional()) {
                    buf.append("\n");
                    createBuilderRow(buf, clzName, attrName, typeName, pattern, "", targetRelevancy, spacer);
                } else {
                    if (targetRelevancy.equals(Constants.TARGET_RELEV_PL)||targetRelevancy.equals(Constants.TARGET_RELEV_KE)) {
                        buf.append("\n" + spacer + ".set" + clzName + "(" + clzName + ".newBuilder().build())");
                        if ((attrList.get(clzName).getType().equals("LocalTimestampMicros") || attrList.get(clzName).getType().equals("LocalTimestampMicrosFixLowValue"))) {
                            buf.append("\n" + spacer + ".set" + clzName + "Millis(" + clzName + "Millis.newBuilder().build())");
                        }
                    }
                }
            }
        }
        if (targetRelevancy.equals(Constants.TARGET_RELEV_KE)||targetRelevancy.equals(Constants.TARGET_RELEV_HE) ) {
            spacer = "      ";
            buf.append("\n"+spacer+".build())");
        }

        if (buildCompletion) {
            buf.append(".build();\n");
        }
    }

    private void generateOptionalAttributeInit(StringBuffer buf, String className, String targetRelevancy) {
        Iterator<String> attributeIt = attrList.keySet().iterator();
        String spacer = "      ";
        while (attributeIt.hasNext()) {
            String clzName = attributeIt.next();
            String attrName = clzName.substring(0, 1).toLowerCase() + clzName.substring(1);
            String typeName = attrList.get(clzName).getType();
            String pattern = attrList.get(clzName).getPattern();

            if (attrList.get(clzName).getTargetRelev().equals(targetRelevancy)) {
                if (attrList.get(clzName).isOptional()) {
                    buf.append("\n");
                    if (attrList.get(clzName).getType().equals("LocalTimestampMicrosFixLowValue"))
                        buf.append("   if (" + attrName + " !=null && !" + attrName + ".equals(\"\") ) {\n");
                    else if (attrList.get(clzName).getType().equals("String"))
                        buf.append("   if (" + attrName + " !=null && !" + attrName + ".equals(\"\") ) {\n");
                    else
                        buf.append("   if (" + attrName + " !=null) {\n");
                    String startPart = "      ";

                    if (targetRelevancy.equals(Constants.TARGET_RELEV_PL)||targetRelevancy.equals(Constants.TARGET_RELEV_KE)) {
                        startPart += className.toLowerCase() + " = " + className + "Domain.newBuilder(" + className.toLowerCase() + ")\n";
                    } else if (targetRelevancy.equals(Constants.TARGET_RELEV_HE)) {
                        startPart += "header = Header.newBuilder(header)\n";
                    }

                    createBuilderRow(buf, clzName, attrName, typeName, pattern, startPart,targetRelevancy,spacer);
                    buf.append(".build();\n");
                    buf.append("   }\n");
                }
            }
        }
        if (targetRelevancy.equals(Constants.TARGET_RELEV_HE)) {
            buf.append(spacer+className.toLowerCase()+"  = "+className+"Domain.newBuilder("+className.toLowerCase()+").setHeader(header).build();\n");
        }
    }


    private void createBuilderRow(StringBuffer buf, String clzName, String attrName, String typeName, String pattern,
                                  String startPart, String targetRelevancy, String spacer) {
        if (!typeName.equals("LocalTimestampMicrosFixLowValue"))
            buf.append(startPart);
        switch (targetRelevancy) {
            case Constants.TARGET_RELEV_PL:
            case Constants.TARGET_RELEV_KE:
                switch (typeName) {
                    case "Date":
                    case "DateFixHL":
                        buf.append(spacer+".set" + clzName + "(" + clzName + ".newBuilder().setValue((int)ChronoUnit.DAYS.between(LocalDate.ofEpochDay(0)," + attrName + ")).build())");
                        break;
                    case "BigDecimal":
                        buf.append(spacer+".set" + clzName + "(" + clzName + ".newBuilder().setValue(" + attrName + ").build())");
                        break;
                    case "TimeMillis":
                        buf.append(spacer+".set" + clzName + "(" + clzName + ".newBuilder().setValue(((int)" + attrName + ".toNanoOfDay()/1000000)).build())");
                        break;
                    case "TimeMicros":
                        buf.append(spacer+".set" + clzName + "(" + clzName + ".newBuilder().setValue((" + attrName + ".toNanoOfDay()/1000)).build())");
                        break;
                    case "TimestampMillis":
                        buf.append(spacer+".set" + clzName + "(" + clzName + ".newBuilder().setValue(GeneratorHelper.getMillisFromDateTime(" + attrName + ")).build())");
                        break;
                    case "LocalTimestampMillis":
                        buf.append(spacer+".set" + clzName + "(" + clzName + ".newBuilder().setValue(GeneratorHelper.getMillisFromDateTimeTZ(" + attrName + ", zoneId)).build())");
                        break;
                    case "TimestampMicros":
                        buf.append(spacer+".set" + clzName + "Millis(" + clzName + "Millis.newBuilder().setValue(GeneratorHelper.getMillisFromDateTime(" + attrName + ")).build())\n");
                        buf.append(spacer+".set" + clzName + "(" + clzName + ".newBuilder().setValue(GeneratorHelper.getMicroFromString(" + attrName + ")).build())");
                        break;
                    case "LocalTimestampMicros":
                        buf.append(spacer+".set" + clzName + "Millis(" + clzName + "Millis.newBuilder().setValue(GeneratorHelper.getMillisFromDateTimeTZ(" + attrName + ", zoneId)).build())\n");
                        buf.append(spacer+".set" + clzName + "(" + clzName + ".newBuilder().setValue(GeneratorHelper.getMicroFromStringTZ(" + attrName + ",zoneId)).build())");
                        break;
                    case "LocalTimestampMicrosFixLowValue":
                        buf.append(spacer+"  LocalDateTime " + attrName + "DT;\n");
                        // TODO Improve for other patterns not starting with year
                        buf.append(spacer+" if (" + attrName + ".startsWith(\"0000\"))\n");
                        buf.append(spacer+"     " + attrName + "DT = LocalDateTime.parse(\"0001-01-01-00.00.00.000000\",DateTimeFormatter.ofPattern(\"yyyy-MM-dd-HH.mm.ss.SSSSSS\"));\n");
                        buf.append(spacer+" else\n");
                        buf.append(spacer+"     " + attrName + "DT = LocalDateTime.parse(" + attrName + ",DateTimeFormatter.ofPattern(\"" + pattern + "\"));\n");
                        buf.append(startPart);
                        buf.append(spacer+".set" + clzName + "Millis(" + clzName + "Millis.newBuilder().setValue(GeneratorHelper.getMillisFromDateTimeTZ(" + attrName + "DT, zoneId)).build())\n");
                        buf.append(spacer+".set" + clzName + "(" + clzName + ".newBuilder().setValue(GeneratorHelper.getMicroFromString(" + attrName + "DT)).build())");
                        break;
                    default:
                        buf.append(spacer+".set" + clzName + "(" + clzName + ".newBuilder().setValue(this." + attrName + ").build())");
                        break;
            }
                break;
            case Constants.TARGET_RELEV_HE:
                switch (typeName) {
                    case "Date":
                    case "DateFixHL":
                        buf.append(spacer+".set" + clzName+"(");
                        buf.append("(int)ChronoUnit.DAYS.between(LocalDate.ofEpochDay(0))," + attrName+")");
                        break;
                    case "TimeMillis":
                        buf.append(spacer+".set" + clzName+"(");
                        buf.append( attrName + ".toNanoOfDay()/1000000)");
                        break;
                    case "TimeMicros":
                        buf.append(spacer+".set" + clzName+"(");
                        buf.append(attrName + ".toNanoOfDay()/1000)");
                        break;
                    case "TimestampMillis":
                        buf.append(spacer+".set" + clzName+"(");
                        buf.append("GeneratorHelper.getMillisFromDateTime(" + attrName + "))");
                        break;
                    case "LocalTimestampMillis":
                        buf.append(spacer+".set" + clzName+"(");
                        buf.append("GeneratorHelper.getMillisFromDateTimeTZ(" + attrName + ", zoneId))");
                        break;
                    case "TimestampMicros":
                        buf.append(spacer+".set" + clzName + "Millis(");
                        buf.append("GeneratorHelper.getMillisFromDateTime(" + attrName + "))");
                        buf.append(spacer+".set" + clzName + "(");
                        buf.append("GeneratorHelper.getMicroFromString(" + attrName + "))");
                        break;
                    case "LocalTimestampMicros":
                        buf.append(spacer+".set" + clzName + "Millis(");
                        buf.append("GeneratorHelper.getMillisFromDateTimeTZ(" + attrName + ", zoneId))");
                        buf.append(spacer+".set" + clzName + "(");
                        buf.append("GeneratorHelper.getMicroFromStringTZ(" + attrName + ",zoneId))");
                        break;
                    default:
                        buf.append(spacer+".set" + clzName + "(");
                        buf.append( attrName + ")");
                        break;
                }
                break;
        }
    }

}
