package net.cloudburo.hexagon.demo.generator;

import java.util.HashMap;

public class GeneratorAggregationBase {

    protected HashMap<String, BindyInterfaceSpec> attrList = null;

    protected String getAttributeName(BindyInterfaceSpec rec, boolean lower) {
        if ((rec.getTargetName()!=null &&(rec.getTargetName().equals("")))) {
            if (lower)
                return rec.getAttributeName().substring(0, 1).toLowerCase() + rec.getAttributeName().substring(1);
            else
                return rec.getAttributeName().substring(0, 1).toUpperCase() + rec.getAttributeName().substring(1);
        }
        else {
            if (lower)
                return rec.getTargetName().substring(0, 1).toLowerCase() + rec.getTargetName().substring(1);
            else
                return rec.getTargetName().substring(0, 1).toUpperCase() + rec.getTargetName().substring(1);
        }
    }
}
