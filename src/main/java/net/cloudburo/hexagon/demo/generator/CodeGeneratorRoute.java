package net.cloudburo.hexagon.demo.generator;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.StringTokenizer;
import java.util.function.Predicate;

@Component
public class CodeGeneratorRoute extends RouteBuilder {

    private static final Logger logger = LoggerFactory.getLogger(CodeGeneratorRoute.class);

    @Autowired
    private CodeGeneratorConfig generatorConfig;
    
    @Override
    public void configure() throws Exception {
        from("file://assets/interfacespec/inbound/?delete=true").routeId("code-generator-route")
            .process(new Processor() {
                @Override
                public void process(Exchange exchange) throws Exception {
                    String className = exchange.getIn().getHeader("CamelFileName", String.class);
                    StringTokenizer tok = new StringTokenizer(className,".");
                    String id = tok.nextToken();
                    List<GeneratorDomainObject> domains = generatorConfig.getDomain();
                    Predicate<GeneratorDomainObject> byId = obj -> obj.getId().equals(id);
                    GeneratorDomainObject cfg = domains.stream().filter(byId).findFirst().get();
                    exchange.getIn().setHeader(Constants.GEN_CFG,cfg);
                }
            })
            .log("Code Generation")
            .to("direct://generateAvroSchema",
                "direct://generateBindyJavaClass",
                "direct://generateKernelJavaClass");
    }



}
