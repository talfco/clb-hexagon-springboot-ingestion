package net.cloudburo.hexagon.demo.generator;

public class GeneratorDomainObject {

    private String id;
    private String className;
    private String packageName;
    private String packagePostfix;
    private String avroNamespace;
    private String fileFormat;
    private String kernelPackageName;

    private String fixedLenPaddingChar;
    private String csvSeparator;
    private boolean skipHeader;
    private boolean skipFooter;
    private boolean ignoreMissingChars;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getAvroNamespace() {
        return avroNamespace;
    }

    public void setAvroNamespace(String avroNamespace) {
        this.avroNamespace = avroNamespace;
    }

    public String getFileFormat() {
        return fileFormat;
    }

    public void setFileFormat(String fileFormat) {
        this.fileFormat = fileFormat;
    }

    public String getFixedLenPaddingChar() {
        return fixedLenPaddingChar;
    }

    public void setFixedLenPaddingChar(String fixedLenPaddingChar) {
        this.fixedLenPaddingChar = fixedLenPaddingChar;
    }

    public boolean isSkipHeader() {
        return skipHeader;
    }

    public void setSkipHeader(boolean skipHeader) {
        this.skipHeader = skipHeader;
    }

    public boolean isSkipFooter() {
        return skipFooter;
    }

    public void setSkipFooter(boolean skipFooter) {
        this.skipFooter = skipFooter;
    }

    public boolean isIgnoreMissingChars() {
        return ignoreMissingChars;
    }

    public void setIgnoreMissingChars(boolean ignoreMissingChars) {
        this.ignoreMissingChars = ignoreMissingChars;
    }

    public String getCsvSeparator() { return csvSeparator; }

    public void setCsvSeparator(String csvSeparator) { this.csvSeparator = csvSeparator; }

    public String getKernelPackageName() {
        return kernelPackageName;
    }

    public void setKernelPackageName(String kernelPackageName) {
        this.kernelPackageName = kernelPackageName;
    }

    public String getPackagePostfix() {
        return packagePostfix;
    }

    public void setPackagePostfix(String packagePostfix) {
        this.packagePostfix = packagePostfix;
    }
}
